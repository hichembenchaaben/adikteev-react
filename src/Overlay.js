import React, { Component } from 'react';
import AdContent from './Ad';
import {configurations} from './configurations';
import styles from './App.css';
import classNames from 'classNames';
import  {getDataLocalStorage, setDataLocalStorage} from './localStorageHelper';

const lsNameSpace = `${configurations.lsNameSpace}.${configurations.lsDemoKey}`;

export default class Overlay extends Component {

  constructor() {
    super();
    this.state = {
      demoBoxOpened: true,
      stopDisplayDemo: false,
      showAd: false,
      demoCount: getDataLocalStorage(lsNameSpace) || 0
    };
  }

  componentDidMount() {
    // increase the views of the modal item
    this.increaseViewCount();
  }

  increaseViewCount() {
    // increase the view count of the
    let count = this.state.demoCount;
    count = parseInt(count, 10) + 1;
    if (count > configurations.maxCount) {
      this.hideDemoBox();
    }
    // set data in the local storage..
    setDataLocalStorage(lsNameSpace, count);
  }

  hideDemoBox() {
    this.setState({demoBoxOpened: false});
    this.setState({showAd: true});
  }

  render() {

    return (
      <div>
        {
          this.state.demoBoxOpened &&
          <div onClick={this.hideDemoBox.bind(this)} className={styles.overlay}></div>
        }
        <div className={styles.adContent}>
          The content of this area goes here...
          {this.state.showAd && <AdContent/>}
        </div>
        {
          this.state.demoBoxOpened &&
          <div className={styles.arrowBox}>
            Click here to launch the ad
            <button className={styles.buttonDemo} onClick={this.hideDemoBox.bind(this)}>I got it</button>
          </div>
        }
      </div>
    );
  }
}
