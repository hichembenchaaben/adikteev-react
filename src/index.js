import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import styles from './App.css';

ReactDOM.render(<App className="styles.rootElement" />, document.getElementById('root'));
