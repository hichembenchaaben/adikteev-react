import React, { Component } from 'react';
import Overlay from './Overlay';
import styles from './App.css';

export default class App extends Component {
  constructor() {
    super();
  }

  render() {
    return (
      <div className={styles.rootElement}>
        <Overlay />
      </div>
    );
  }
}
