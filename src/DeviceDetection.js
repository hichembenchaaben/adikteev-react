import parser from 'ua-parser-js';
const par = new parser().getResult();
export function isDesktop() {
  // device is undefined means it's a desktop client
  return par.device.type ? false : true;
}
