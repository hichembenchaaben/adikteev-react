import {configurations} from './configurations';

export function setDataLocalStorage(key, value) {
  if (key) {
    localStorage.setItem(key, value);
  } else {
    console.error('you need to specify a key for the local storage');
  }
}

/**
 * get data form the local storage using a key param
 */
export function getDataLocalStorage(key) {
  let item = localStorage.getItem(key);
  if (!item || item === null || isNaN(item)) {
    return undefined;
  }
  try {
    return JSON.parse(item);
  } catch (e) {
    return item;
  }
}
