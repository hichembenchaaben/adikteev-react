import React, {Component} from 'react';
import styles from './App.css';
import {isDesktop} from './DeviceDetection';
import classNames from 'classnames/bind';
let cx = classNames.bind(styles);

export default class AdContent extends Component {

  constructor() {
    super();
    this.state = {
      canShow: false,
      isShown: false
    }
  }

  componentWillMount() {
    if (isDesktop) {
      this.setState({canShow: true, isShown: true});
    }
  }

  closeMe() {
    this.setState({isShown: !this.state.isShown});
  }

  render() {
    let adClasses = cx({
      hideM1Content: !this.state.isShown,
      showM1Content: this.state.isShown,
      m1Container: true
    });

    return (
      <div>
        {
          this.state.canShow &&
          <div onClick={this.closeMe.bind(this)} className={adClasses}>
            <div className={styles.m1Content}>
              {this.state.isShown &&
                <p>Whatever content goes here...</p>
              }
              {!this.state.isShown &&
                <p>I'm iddle now</p>
              }
            </div>
          </div>
        }
      </div>
    );
  }
}
