// application runtime constants
export const configurations = {
  lsNameSpace: 'adk',
  numberOfDemoDisplays: 10,
  maxCount: 10,
  lsDemoKey: 'demoCount'
}
