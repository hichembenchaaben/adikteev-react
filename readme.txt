Salut Ichem,
Merci pour l'entretien. Quentin et moi avons bien apprécié ton profil et on pense que tu pourrais apporter beaucoup à Adikteev avec ton entrain et ton expérience. De ton coté, je pense qu'Adikteev peut t'apporter un environnement de travail jeune et dynamique dans une startup en pleine croissance où tu pourrais explorer tous les domaines du Front car nous sommes toujours en demande de nouvelles technos et de nouveaux formats.

On va donc continuer dans le process de recrutement.
J'attends mardi pour voir qui pourrait participer à ton second entretien (avec les congés ce n'est pas évident).

J'aimerais aussi de faire faire un test qui mixe de l'inté avec du build JS.
Le test est le suivant :
- Création d'un projet qui build un script JS
- Disposer d'une page test qui charge ton script avec une simple balise <script>
- Le script doit afficher un format M-Impact qui s'afficherait sur la droite de l'écran (et non à gauche) avec une animation CSS. Pas nécessaire de développer le reste du format.
Voici un exemple :  http://demo.adikteev.com/M-Impact
Ce que l'on veut juger : ta capacité à faire un projet de build et la qualité de ton code HTML / CSS.

Ok pour toi ? 
Cordialement,
Frederic Leroy
