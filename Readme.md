# Adikteev assignment

#### Instructions

J'aimerais aussi de faire faire un test qui mixe de l'inté avec du build JS.
Le test est le suivant :
- Création d'un projet qui build un script JS
- Disposer d'une page test qui charge ton script avec une simple balise <script>
- Le script doit afficher un format M-Impact qui s'afficherait sur la droite de l'écran (et non à gauche) avec une animation CSS. Pas nécessaire de développer le reste du format.
Voici un exemple :  http://demo.adikteev.com/M-Impact
Ce que l'on veut juger : ta capacité à faire un projet de build et la qualité de ton code HTML / CSS.


### Solution

Le script a été implementé avec la tech stack suivante

- React ES6 / babel
- Wepack (dev / build)
- CSS Modules
- Node et webpack pour le build
- npm

Note, dans un project reel on pourrai choisir un starter project minimaliste et efficate.
Voici une liste d'exemples:
http://andrewhfarmer.com/starter-project/

### Installation
```
npm install
```

### Lancer un serveur Dev HMR (webpack dev server + hot module replacement)

```
npm start
```

le format s'ouvre directement apres avoir lance la commande.

Pour ouvrir un example (comme le site), naviguez vers le lien suivant:
```
http://localhost:3000/example/index.html
```


### Build

Exécuter la commande suivante pour avoir le build javascript du format

```
node build production
```

Le build est un script node qui supporte aussi le node sur les machines Windows.


### Détails du dev

##### Approche

Pour intégrer le format, il faut mettre la balise `<script src="bundle.js"></script>`
dans la page, le contenu va se charger dans une `<div id="root">`.

Juste pour simplifier l'éxercice, j'ai choisi de mettre la div cible manuellement
dans la page, le contenu va etre ensuite chargé entierement par `bundle.js`

##### Détection du user agent

Afin de détecter et targetter le client, j'ai utilise un script base sur userer agent detection.
Qui est le plus lightweight possible.

Ce script permet de détecter les info nécessaires sur le browser client (desktop / tablet / mobile)
Ainsi que la plateforme (Windows / IOS) et le type du browser (Safari / Chrome) etc..

##### Le Build

Un script node est chargé en conjunction avec webpack de créer un seul script qui contient:

- HTML (jsx)
- CSS
- Librairies ()

Le build est basé sur webpack2, qui utilise 'Tree shaking', c'est a dire qui inclus seulement
les modules qui ont ete importés par ES6.

[plus de details sur webpack2 tr shaking](http://www.2ality.com/2015/12/webpack-tree-shaking.html)

Le resultat, un seul script de taille 256kb qui contient le format.

##### Le format (React + CSS modules)

Webpack css modules aident a utiliser des classes CSS avec une specifité particuliere.
Afin de ne pas faire des collisions avec le CSS qui existe deja (site client) et manager facilement le versionnement
du format.

1- Le CSS

Toutes les classes css possedent un namespace + module + hash.
Soit `adk__[local]___[hash:base64:5]`.
donc on peut savoir quel namespace (adkd),  module et hash si on l'utilise pour le versionnement ou au moins
pour ne pas avoir deux differnts formats avec les memes classes.

2- config de l'application React

React utilise `configurations.js` pour les constantes de l'application.
J'ai choisi d'ajouter un nombre limité de l'affichage du popup demo (juste un example et pas necessairement requis dans un vrai use case).

3- Localstorage helper

L'application utilise un script local storage pour persister certaines donnés (potentiellement utile dans les components avec state)
Le script utilise un namespace pour ne pas réecrire accidentellement une key utilisé par un autre script.

4- Detection du client (browser desktop/tablette / mobile)
J'ai choisi d'utiliser le module npm ua-parser-js, c'est lightweight et minmaliste.
Je me suis limite a detecter si le client ouvre le format sur desktop ou pas.
Le format ne va pas s'afficher sur les tablettes et les mobiles.

